from odoo import http, _
from odoo.http import request
from odoo.addons.lc_rest_api.controllers.controllers import validate_token, generate_response, getRequestIP, serialization_data, _args,toUserDatetime, permission_check
import json
import datetime
class getBillsData(http.Controller):
    @validate_token
    @http.route(['/api/bill/imexs/'], methods=['GET'], type='http', auth='none', csrf=False)
    def getData(self, access_token, **kw):
        if not permission_check('purchase.order', 'read'):
            return generate_response(data={
                'code': 0,
                'message': "Permission denied"
            })
        try:
            _domain, _fields, _offset, _limit, _order = _args(kw)
            res = []
            finalRes = []
            if kw.get('icpp'):
                _limit = int(kw['icpp'])
            purchases = request.env['purchase.order'].search_read(fields=[], offset=_offset, limit=_limit, order=_order)
            partners = request.env['res.partner'].search_read()
            products = request.env['product.product'].search_read()
            sales = request.env['sale.order'].search_read(fields=[], offset=_offset, limit=_limit, order=_order)
            for i in range(len(purchases)):
                listTerm={
                    'id': purchases[i]['id'],
                    'billId': purchases[i]['invoice_ids'],
                    'date': purchases[i]['date_approve'],
                    'depotId': purchases[i]['picking_type_id'][0],
                    'depotName': purchases[i]['picking_type_id'][1],
                    'supplierId': purchases[i]['partner_id'][0],
                    'supplierName': purchases[i]['partner_id'][1],
                    'supplierMobile': partners[purchases[i]['partner_id'][0]]['phone'],
                    'proudctStore': [{
                        'code': products[purchases[i]['order_line'][j]]['id'],
                        'barcode': products[purchases[i]['order_line'][j]]['barcode'],
                        'name': products[purchases[i]['order_line'][j]]['display_name'],
                        'importPrice': products[purchases[i]['order_line'][j]]['price'],
                        'price': products[purchases[i]['order_line'][j]]['price']
                    }for j in range(len(purchases[i]['order_line']))],
                    'avgCost': purchases[i]['amount_total'],
                    'productPrice': purchases[i]['amount_total'],
                    'description': purchases[i]['notes'],
                    'createdDateTime': purchases[i]['create_date']
                }
                res.append(listTerm)
            for i in range(len(sales)):
                listTerm={
                    'id': sales[i]['id'],
                    'billId': sales[i]['invoice_ids'],
                    'date': sales[i]['confirmation_date'],
                    'depotId': sales[i]['warehouse_id'][0],
                    'depotName': sales[i]['warehouse_id'][1],
                    'customerId': sales[i]['partner_id'][0],
                    'customerName': sales[i]['partner_id'][1],
                    'customerMobile': partners[sales[i]['partner_id'][0]]['phone'],
                    'proudctStore': [{
                        'code': products[sales[i]['order_line'][j]]['id'],
                        'barcode': products[sales[i]['order_line'][j]]['barcode'],
                        'name': products[sales[i]['order_line'][j]]['display_name'],
                        'importPrice': products[sales[i]['order_line'][j]]['price'],
                        'price': products[sales[i]['order_line'][j]]['price']
                    }for j in range(len(sales[i]['order_line']))],
                    'avgCost': sales[i]['amount_total'],
                    'productPrice': sales[i]['amount_total'],
                    'description': sales[i]['note'],
                    'createdDateTime': sales[i]['create_date']
                }
                res.append(listTerm)

            # filter by id 
            if kw.get('id'):
                for i in range(len(res)):
                    if int(serialization_data(res)[i]['id'])==int(kw['id']):
                        finalRes.append(serialization_data(res)[i])

            # filter by fromDate and toDate
            # if kw.get('fromDate') and kw.get('toDate'):
            #     for i in range(len(res)):
            #         if int(serialization_data(res)[i]['date'])>=int(kw['fromDate']) and int(serialization_data(res)[i]['date'])<=int(kw['toDate']):
            #             finalRes.append(serialization_data(res)[i])
            # else:
            #     finalRes = res

            # filter by billId
            if kw.get('billId'):
                for i in range(len(res)):
                    if int(serialization_data(res)[i]['billId'])==int(kw['billId']):
                        finalRes.append(serialization_data(res)[i])

            # filter by depotId 
            if kw.get('depotId'):
                for i in range(len(res)):
                    if int(serialization_data(res)[i]['depotId'])==int(kw['depotId']):
                        finalRes.append(serialization_data(res)[i])


            if not kw.get('id') and not kw.get('billId') and not kw.get('depotId'):
                finalRes = res
            # filter by page
            if kw.get('page'):
                pageFrom = int(kw['page'])
                pageTo = int(kw['page'])+1
            else:
                pageFrom = 0
                pageTo = len(finalRes)

            return generate_response(data={
                'code': 1,
                'message': "Success",
                'data':[{
                    'totalPage': pageTo-pageFrom,
                    'page': i,
                    'Imexs': serialization_data(finalRes)[i]
                }for i in range(pageFrom,pageTo)]
            })
        except Exception as error:
            return generate_response(data={
                'code': 0,
                'message': "{}".format(error)
            })